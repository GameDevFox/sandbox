import { Split, renderLoop } from '@gamedevfox/katana';
import React, { useEffect, useRef } from 'react';

export const List = maxSize => {
  const items = [];
  const [input, output] = Split();

  const result = (...args) => {
    if(args.length === 0)
      return items;

    const [item] = args;
    items.push(item);

    if(items.length > maxSize)
      items.shift();

    input(items);
  };

  result.output = output;

  return result;
};

export const FrameView = props => {
  const { renderFn } = props;

  const divRef = useRef(null);

  useEffect(() => {
    let value = '';
    return renderLoop(time => {
      const newValue = renderFn(time);
      if(value !== newValue) {
        value = newValue;
        divRef.current.innerHTML = value;
      }
    });
  }, [renderFn]);

  const otherProps = { ...props };
  delete otherProps.renderFn;

  return (
    <div {...otherProps} ref={divRef}/>
  );
};

export const forTheta = (arr, fn) => {
  const { length } = arr;
  for(let i = 0; i < length; i++) {
    const theta = i / (length - 1);
    fn(arr[i], theta);
  }
};

export const toCircle = radius => distance => {
  const circumference = radius * Math.PI;
  const theta = distance / circumference;
  const radians = theta * (Math.PI * 2);
  return { x: Math.cos(radians) * radius, y: Math.sin(radians) * radius };
};
