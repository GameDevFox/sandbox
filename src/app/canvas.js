import { renderLoop } from '@gamedevfox/katana';
import React, { useEffect, useRef } from 'react';

export const applyStyle = (style, context2d) => {
  Object.entries(style).forEach(([key, value]) => {
    context2d[key] = value;
  });
};

export const clear = ({ canvas, context2d }) => {
  const { clientWidth: width, clientHeight: height } = canvas;
  context2d.clearRect(0, 0, width, height);
};

export const drawBorder = ({ canvas, context2d }) => {
  const { clientWidth: width, clientHeight: height } = canvas;

  const lineWidth = 5;
  context2d.lineWidth = lineWidth;
  context2d.lineJoin = 'miter';
  context2d.strokeStyle = 'black';

  const halfLineWidth = lineWidth / 2;
  context2d.strokeRect(halfLineWidth, halfLineWidth, width - lineWidth, height - lineWidth);
};

export const Canvas = props => {
  const { render } = props;

  const canvasRef = useRef(null);

  useEffect(() => {
    const canvas = canvasRef.current;

    const renderFn = render(canvas);
    return renderLoop(renderFn);
  }, [render]);

  const otherProps = { ...props };
  delete otherProps.render;

  return (
    <canvas ref={canvasRef} {...otherProps}/>
  );
};

const drawAxisLine = ({ context2d, expandPointFn, value, style = {}, vertical = false }) => {
  applyStyle(style, context2d);

  let start, end;
  if(vertical) {
    start = expandPointFn([value, 0]);
    end = expandPointFn([value, 1]);
  } else {
    start = expandPointFn([0, value]);
    end = expandPointFn([1, value]);
  }

  context2d.beginPath();
  context2d.lineTo(...start);
  context2d.lineTo(...end);
  context2d.stroke();
};

export const AxisDrawer = (reducePoint, expandPoint, style = {}) => ({ context2d }) => {
  context2d.lineWidth = 4;
  context2d.lineJoin = 'bevel';
  context2d.strokeStyle = 'black';

  const expandPointFn = expandPoint();
  const origin = reducePoint()([0, 0]);

  // X axis
  if(origin[1] >= 0 && origin[1] <= 1)
    drawAxisLine({ context2d, expandPointFn, value: origin[1], style });

  // Y axis
  if(origin[0] >= 0 && origin[0] <= 1)
    drawAxisLine({ context2d, expandPointFn, value: origin[0], style, vertical: true });
};

export const GridDrawer = ({ reducePoint, expandPoint, interval = 1, style = {}, vertical = true }) => {
  let anchor = 0;

  return ({ context2d }) => {
    context2d.lineWidth = 1;
    context2d.lineJoin = 'bevel';
    context2d.strokeStyle = 'black';
    context2d.globalAlpha = 0.2;

    const reducePointFn = reducePoint();
    const expandPointFn = expandPoint();

    let cursor = anchor;

    // Forward
    let value = 0;
    while(value < 1) {
      const point = vertical ? [cursor, 0] : [0, cursor];
      value = reducePointFn(point)[vertical ? 0 : 1];

      if(value > 0) {
        anchor = cursor;
        drawAxisLine({ context2d, expandPointFn, value, style, vertical });
      }

      cursor += interval;
    }

    // Backward
    cursor = anchor;

    value = 1;
    while(value > 0) {
      const point = vertical ? [cursor, 0] : [0, cursor];
      value = reducePointFn(point)[vertical ? 0 : 1];

      if(value < 1) {
        anchor = cursor;
        drawAxisLine({ context2d, expandPointFn, value, style, vertical });
      }

      cursor -= interval;
    }

    context2d.globalAlpha = 1;
  };
};

export const PointDrawer = (points, project, style = {}) => {
  return ({ context2d }) => {
    context2d.lineWidth = 2;
    context2d.lineJoin = 'bevel';
    context2d.strokeStyle = 'black';

    applyStyle(style, context2d);

    context2d.beginPath();
    points().forEach(point => {
      const [x, y] = project(point);
      context2d.lineTo(x, y);
    });
    context2d.stroke();
  };
};
