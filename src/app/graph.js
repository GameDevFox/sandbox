/* eslint-disable */
import { ComplexValue, Keys, Range, Value, reduce, expand, renderLoop } from '@gamedevfox/katana';
import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

import { clear, drawBorder, AxisDrawer, Canvas, GridDrawer, PointDrawer } from "./canvas";
import { List, FrameView } from './utils';

const Styles = styled.div`
  border: 2px solid red;
  canvas {
    width: 100%;
    height: 400px;
  }
`;

const pointFn = fn => ({ rangeX }) => point => {
  return rangeX().map((range, index) => fn(point[index], range[0], range[1]));
};

const extendView = (rangeX, point) => {
  for(let i=0; i < point.length; i++)
    rangeX[i].extend(point[i]);
};

const Projection = (reduceFn, expandFn) => point => {
  const normal = reduceFn()(point);
  return expandFn()(normal);
};

const keys = Keys();
window.keys = keys;

keys('KeyK').output(down => (down && console.log(keys())));

const [w, a, s, d] = 'WASD'.split('').map(letter => keys(`Key${letter}`));

const x = ComplexValue(({ up, down }) => {
  return (up() ? 1 : 0) + (down() ? -1 : 0);
}, { up: w, down: s });

// =============================================================================

const canvas = Value();

const canvasWidth = Value(0);
const canvasHeight = Value(0);

const points = List(150);
const otherPoints = List(150);
const keyPoints = List(150);

const canvasView = ComplexValue(
  ({ width, height }) => [[0, width()], [0, height()]],
  { width: canvasWidth, height: canvasHeight }
);

const inputRange2 = ComplexValue(({ points, otherPoints, keyPoints }) => {
  const range2 = [Range(), Range()];

  const boundExtendView = point => extendView(range2, point);
  [points(), otherPoints(), keyPoints()].forEach(pnts => pnts.forEach(boundExtendView));

  const [xRange, yRange] = range2;

  yRange.extend(-1.1);
  yRange.extend(1.1);

  if(xRange[1] - xRange[0] > 50)
    xRange[0] = xRange[1] - 50;

  return range2;
}, { points, otherPoints, keyPoints });

const outputRange2 = ComplexValue(({ canvasView }) => {
  const [width, height] = canvasView().map(range => range[1]);
  return [
    [0, width],
    [height, 0],
  ];
}, { canvasView });

const reducePoint = ComplexValue(pointFn(reduce), { rangeX: inputRange2 });
const expandPoint = ComplexValue(pointFn(expand), { rangeX: outputRange2 });

const project = Projection(reducePoint, expandPoint);

const drawAxis = AxisDrawer(reducePoint, expandPoint);
const drawHGrid = GridDrawer({ reducePoint, expandPoint, interval: 0.5, vertical: false });
const drawVGrid = GridDrawer({ reducePoint, expandPoint, interval: Math.PI / 4, vertical: true });

const drawPoints = PointDrawer(points, project, { strokeStyle: 'red' });
const drawOtherPoints = PointDrawer(otherPoints, project, { strokeStyle: 'blue' });
const drawKeyPoints = PointDrawer(keyPoints, project, { strokeStyle: '#00ff00', lineWidth: 3 });

const renderFn = ComplexValue(({ canvas }) => {
  if(!canvas())
    return () => {};

  const context2d = canvas().getContext('2d');
  const doublePI = Math.PI * 2;

  const random = Math.floor(Math.random() * 100);
  return time => {
    const { clientWidth, clientHeight } = canvas();
    if(canvasWidth() != clientWidth) {
      canvas().width = clientWidth;
      canvasWidth(clientWidth);
    }
    if(canvasHeight() != clientHeight) {
      canvas().height = clientHeight;
      canvasHeight(clientHeight);
    }

    const offsetTime = time - Math.PI * 2;
    const radians = time % doublePI;

    points([offsetTime, Math.cos(radians)]);
    otherPoints([offsetTime, Math.sin(radians)]);
    keyPoints([offsetTime, x()]);

    [
      clear, drawAxis, drawHGrid, drawVGrid,
      drawPoints, drawOtherPoints, drawKeyPoints,
      drawBorder,
    ].map(fn => fn({ canvas: canvas(), context2d, time }));
  };
}, { canvas });

let clearRenderLoop;
renderFn.output(renderFn => {
  if(!canvas()) {
    if(clearRenderLoop) {
      clearRenderLoop()
      clearRenderLoop = null;
    }

    return;
  }

  clearRenderLoop = renderLoop(renderFn);
});

export const Graph = () => {
  const canvasRef = useRef(null);

  useEffect(() => {
    canvas(canvasRef.current);
    return () => canvas(null);
  }, []);

  return (
    <>
      <FrameView style={{ position: 'absolute', top: '20px', left: '10px' }} renderFn={() => {
        return JSON.stringify(inputRange2()[0].map(value => value.toFixed(2)));
      }}/>
      {/*<Canvas render={render}/>*/}
      <canvas ref={canvasRef}/>
    </>
  );
};
