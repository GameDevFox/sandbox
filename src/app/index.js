/* eslint-disable */
import { on, chainFns, FPS, Time, Value } from '@gamedevfox/katana';
import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import { createGlobalStyle } from 'styled-components';

import store from '../store';

import { Graph } from './graph';
import { FrameView } from './utils';

if(window.cleanup)
  window.cleanup();

const cleanups = [];

// const off = on('keydown', (...args) => console.log('Key Me:', ...args));
// cleanups.push(off);

// const myTime = Time();
// myTime.update(time => console.log('Time', time));
// setInterval(() => {
//   myTime(true)
// }, 1000 / 15);

// const myVal = Value(({ time }) => time() * 20, { time: myTime });
// myVal.update(val => console.log('>>>', val));

const myFPS = FPS();

const myRender = time => {
  const { delta, fps } = myFPS(time);
  return `${fps} (this frame: ${Math.floor(delta * 1000)}ms)`;
};

const App = () => {
  const GlobalStyle = createGlobalStyle`
    body { background-color: lightyellow; }
    #root {
      height: 100%;

      display: flex;
      flex-direction: column;

      canvas { flex-grow: 1; }
    }

    .fps {
      position: absolute;
      top: 5px;
      left: 10px;
    }
  `;

  return (
    <Provider store={store}>
      <GlobalStyle/>

      {/*
      <h1>Zion</h1>
      */}
      <FrameView className="fps" renderFn={myRender}/>
      <Graph/>
    </Provider>
  );
};

export default hot(App);

window.cleanup = () => cleanups.forEach(cleanFn => cleanFn());
